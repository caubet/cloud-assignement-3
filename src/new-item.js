import PropTypes from 'prop-types';
import React, { Component } from 'react';
import './App.css';
import Validator from './shared/validator';

class NewItem extends Component {

  static propTypes = {
      onSubmit: PropTypes.func.isRequired,
      onCancel: PropTypes.func.isRequired
  };

  constructor(props) {
    super(props);
    this.validator = new Validator();
    this.onCancel = this.onCancel.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.state = {
      id: '',
      title: '',
      author: '',
      category: ''
    };
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  onCancel() {
    this.props.onCancel();
  }

  onSubmit() {
    if(this.validator.validateInputs(this.state)) {
        this.props.onSubmit(this.state);
    }
  }

  render() {
    return (
      <div className="input-panel">
      <span className="form-caption">New item:</span>
      <div>
        <label className="field-name">Id:<br/>
          <input value={this.state.id} name="id" maxLength="40" required onChange={this.handleInputChange} placeholder="item id" />
        </label>
      </div>
      <div>
        <label className="field-name">Title:<br/>
          <input value={this.state.title} name="title"  required onChange={this.handleInputChange} placeholder="title" />
        </label>
      </div>
      <div>
        <label className="field-name">Author:<br/>
          <input value={this.state.author} name="author" required onChange={this.handleInputChange} placeholder="author" />
        </label>
      </div>
      <div>
        <label className="field-name">category:<br/>
          <input value={this.state.category} name="category" required onChange={this.handleInputChange} placeholder="category" />
        </label>
      </div>
      <br/>
      <button onClick={() => this.onCancel()}>Cancel</button>&nbsp;
      <button onClick={() => this.onSubmit()}>Create</button>
      </div>
    );
  }
}

export default NewItem;
