import React, { Component } from 'react';
import './App.css';

class ItemDetails extends Component {

  constructor(props) {
    super(props);
    this.onEdit = this.onEdit.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  render() {
    const item = this.props.item;
    return (
      <div className="input-panel">
      <span className="form-caption">{item.Item.id}</span>
      <div><span className="field-name">Id:</span><br/> {item.Item.id}</div>
      <div><span className="field-name">Title:</span><br/> {item.Item.title}</div>
      <div><span className="field-name">Author:</span><br/> {item.Item.author}</div>
      <div><span className="field-name">Category:</span><br/> {item.Item.category}</div>
      <br/>
      <button onClick={() => this.onDelete()}>Delete</button>&nbsp;
      <button onClick={() => this.onEdit()}>Edit</button>
      </div>
    );
  }

  onEdit() {
    this.props.onEdit();
  }

  onDelete() {
    const item = this.props.item;
    if(window.confirm("Are you sure to delete item: " + item.Item.id + " ?")) {
      this.props.onDelete(item.Item.id);
    }
  }

}

export default ItemDetails;
