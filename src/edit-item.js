import React, { Component } from 'react';
import './App.css';
import Validator from './shared/validator';

class EditItem extends Component {

  constructor(props) {
    super(props);
    this.validator = new Validator();
    this.onCancel = this.onCancel.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    const itemToEdit = props.item.Item;
    this.state = {
      id: itemToEdit.id,
      title: itemToEdit.title,
      author: itemToEdit.author,
      category: itemToEdit.category,
    };
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }

  onCancel() {
    this.props.onCancel();
  }

  onSubmit() {
    if (this.validator.validateInputs(this.state)) {
      this.props.onSubmit(this.state);
    }
  }

  render() {
    return (
      <div className="input-panel">
      <span className="form-caption">Edit item:</span><span>{this.state.id}</span>
      <div>
        <label className="field-name">Id:<br/>
          <input value={this.state.id} name="id"  required onChange={this.handleInputChange} placeholder="item id" />
        </label>
      </div>
      <div>
        <label className="field-name">Title:<br/>
          <input value={this.state.title} name="title"  required onChange={this.handleInputChange} placeholder="title" />
        </label>
      </div>
      <div>
        <label className="field-name">Author:<br/>
          <input value={this.state.author} name="author" required onChange={this.handleInputChange} placeholder="author" />
        </label>
      </div>
      <div>
        <label className="field-name">category:<br/>
          <input value={this.state.category} name="category" required onChange={this.handleInputChange} placeholder="category" />
        </label>
      </div>

      <br/>
      <button onClick={() => this.onCancel()}>Cancel</button>&nbsp;
      <button onClick={() => this.onSubmit()}>Update</button>
      </div>
    );
  }
}

export default EditItem;
