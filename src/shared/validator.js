class Validator {

  validateInputs(inputData) {
    let errorMsg = "";
    if(!inputData.id) {
      errorMsg +="Please enter id of this item.\n"
    }
    if(!inputData.title) {
      errorMsg +="Please enter title of this item.\n"
    }
    if(!inputData.author) {
      errorMsg +="Please enter author of this item.\n"
    }
    if(!inputData.category) {
      errorMsg +="Please enter category of this item.\n"
    }

    if(errorMsg.length == 0){
      return true;
    } else {
      alert(errorMsg);
      return false;
    }
  }
}

export default Validator;
